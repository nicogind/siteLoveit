---
title: "Publications"
date: 2022-09-06T17:42:45+01:00
draft: false
---

<font size="3"> **Articles de conférence** </font>

N. Gindrier, R. Clackdoyle, S. Rit, and L. Desbat. Cone-beam reconstruction from
n-sin trajectories with transversely-truncated projections. In 6th International Confe-
rence on Image Formation in X-Ray Computed Tomography, pages 46–49, Regens-
burg, 2020 [pdf HAL](https://hal.science/hal-02928137/document)

N. Gindrier, R. Clackdoyle, S. Rit, and L. Desbat. Sufficient field-of-view for the
M-line method in cone-beam CT. In 2020 IEEE Nuclear Science Symposium and
Medical Imaging Conference (NSS/MIC), Boston (virtual), United States, 2020. [pdf HAL](https://hal.science/hal-02928137/document)

N. Gindrier, L. Desbat, and R. Clackdoyle. CB reconstruction for the 3-sin trajectory
with transverse truncation. In 6th Virtual International Meeting on Fully 3D Image
Reconstruction in Radiology and Nuclear Medicine, Leuven, 2021. [pdf HAL](https://hal.science/hal-03604426/document)


[google scholar](https://scholar.google.com/citations?user=ccMEmqwAAAAJ&hl=fr&oi=ao)
