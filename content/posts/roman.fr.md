---
title: "Manuscrit roman"
date: 2024-05-01T21:56:45+01:00
draft: false
description: ""
---
<!--more-->

Voilà le manuscrit de mon roman "La Quintessence des secondes", que je chercherai peut-être à éditer ou auto-éditer (si vous avez des pistes...). 

En attendant, s'il vous venait l'envie de le découvrir, je prends toutes les remarques, coquilles, incohérences...


[manuscritGindrier.pdf](/page-perso/main.pdf)