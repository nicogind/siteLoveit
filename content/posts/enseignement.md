---
title: "Enseignement"
date: 2022-09-05T20:39:02+01:00
draft: false
description: "Résumé des enseignements et documents"
---
<!--more-->

J'ai donné 3 heures de cours de tomographie médicale en 2018 dans le master M2MSIAM (UGA/Ensimag). Cela concernait une introduction à la méthode DBP, une méthode de reconstruction analytique au centre de ma thèse.

Voici les DMs corrigés que j'avais faits, ainsi que le polycopié de cours.


[polycopié_de_cours.pdf](/page-perso/HilbertDBP.pdf)

[DM12corrigé.pdf](/page-perso/DM12correction.pdf)

J'ai également donné des cours/TD/TP en 2020 en MAP101 (introduction à l'analyse et au calcul scientifique) en première année de prépa Polytech Grenoble. 
