---
title: "Thesis"
date: 2022-10-05T21:56:45+01:00
draft: false
description: "Cone-beam reconstruction from projections with transverse truncation"
---
My thesis in applied mathematics, Cone-beam reconstruction from projections with transverse truncation, deals with the problem of tomographic reconstruction with missing data. 

This version is the most up-to-date, some typos are corrected compared to thèses.fr.


[these.pdf](/page-perso/manuscritVF.pdf)
