---
title: "Presentation"
date: 2023-01-03T19:17:35+01:00
draft: false
description: "Presentation of the curriculum and the cv"
---
<!--more-->
After studies in preparatory classes in Nice (kind of Bachelor in the French system), I made an engineering school in Grenoble, called Ensimag. Simultaneously, I made a master in applied maths (MSIAM) in the Grenoble-Alpes University (UGA).

I performed a thesis in applied mathematics in the TIMC laboratory in UGA. 

I am currently in post-doc (since February 2023) in the RICAM laboratory of ÖAW (Austrian academia of sciences). 

[CV.pdf](/page-perso/cv.pdf)

Outside mathematics, I also practice running, guitar, tennis and bouldering.
