---
title: "Présentation"
date: 2023-01-03T19:17:35+01:00
draft: false
description: "Présentation du cursus et CV"
---
<!--more-->
Après des études en classes préparatoires au lycée Masséna à Nice (PCSI-PSI), j'ai effectué un cursus ingénieur à l'ENSIMAG (Grenoble-INP). Lors de mes deuxième et troisième année j'ai suivi le master MSIAM de l'Université Grenoble-Alpes. 

J'ai réalisé une thèse en mathématiques appliquées au sein du laboratoire TIMC de l'Université Grenoble-Alpes (plus d'informations dans le post adéquat), de 2018 à 2022. 

Je suis actuellement (depuis février 2023) en post-doc dans le laboratoire RICAM de l'ÖAW (académie autrichienne des sciences) à Linz. 

[CV.pdf](/page-perso/cv.pdf)

En dehors des mathématiques, je fais aussi de la course à pied (surtout du trail), de la guitare, du tennis et un peu de bloc (bouldering).

