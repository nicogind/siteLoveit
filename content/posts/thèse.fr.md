---

title: "Thèse"
date: 2022-10-05T21:56:45+01:00
draft: false
description: " Reconstruction cone-beam à partir de projections avec troncations transverses"

---

<!--more-->

Ma thèse de mathématiques appliquées, intitulée Reconstruction cone-beam à partir de projections avec troncations transverses, traite du problème de reconstruction tomographique avec des données manquantes.

Cette version est la plus à jour, quelques coquilles y sont corrigées par rapport à la version de thèses.fr.

[these.pdf](/page-perso/manuscritVF.pdf)