---
title: "Tedx"
date: 2021-02-05T21:45:56+01:00
draft: false
description: "participation au tedx UGA 2018"
---
<!--more-->
En 2018, j'ai été sélectionné pour être un invité étudiant du tedX UGA. Je mets le lien de ma prestation youtube, même si de la nuance doit être apportée à ce qui est dit dans ce discours (voir par exemple mon commentaire sur la vidéo).

[tedX](https://www.youtube.com/watch?v=yxJoBIp6ZJE)
